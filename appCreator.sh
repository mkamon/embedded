#! /bin/bash

mkdir application
tar -xzvf application.tar.gz -C ./application
mkdir ./application/deps/
tar -xzvf SDL2.tar.gz -C ./application/deps/
cd application
mkdir build
cd build
cmake -DInstallDir:STRING=build ..
make
make install
