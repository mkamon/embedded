To extract and compile project run appCreator.sh.
Once extracted, to execute test script run runExample.sh.
The binaries will be located in ./application/build/bin directory by default.
The logFile.txt and rendered drawing will be located in ./application/build by default.
Please note, the SDL2 library is pre-compiled under x86_64 architecture.
