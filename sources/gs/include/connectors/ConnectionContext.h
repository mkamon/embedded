#ifndef CONNECTION_CONTEXT_H_
#define CONNECTION_CONTEXT_H_

#include <string>

class ConnectionContext
{
public:
    virtual void sendMessage( const std::string& msg) = 0;
    virtual void readMessage( std::string& msg) = 0;
    virtual ~ConnectionContext() = default;
};


#endif /* CONNECTION_CONTEXT_H_ */