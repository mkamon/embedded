#ifndef DOUBLE_WAY_CONNECTOR_H_
#define DOUBLE_WAY_CONNECTOR_H_

#include <string>
#include <array>

#include "ConnectionContext.h"

template< size_t BuffSize >
class DoubleWayConnector : public ConnectionContext
{
public:
    DoubleWayConnector();
    DoubleWayConnector( int readId, int writeId );

    void getOtherEndRWIds( int& readId, int& writeId );
    void sendMessage( const std::string& msg);
    void readMessage( std::string& msg);

    ~DoubleWayConnector();
private: 

    std::array<char, BuffSize> buff;
    int thisEndRWIds[2];
    int otherEndRWIds[2];
};


#endif /* DOUBLE_WAY_CONNECTOR_H_ */