#ifndef COMMAND_PARSER_H_
#define COMMAND_PARSER_H_
#include <string>
#include <memory>
class PictureCreator;

class DrawingCommand
{
public:
    virtual void execute( PictureCreator* ) = 0;
    virtual ~DrawingCommand() = default;
};

class DrawingCommandParser
{
public:
    static std::shared_ptr<DrawingCommand> parseCmd( const std::string& cmd);
};



#endif /* COMMAND_PARSER_H_ */