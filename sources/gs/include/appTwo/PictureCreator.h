#ifndef PICTURE_CREATOR_H_
#define PICTURE_CREATOR_H_

#include <memory>
#include <map>
class PictureCreatorImpl;
class DrawingCommand;

class PictureCreator
{
public: 
    struct Point { int x,y; };

    PictureCreator();
    void setWidth( int w);
    void setHeight( int h);
    void drawRectangle( Point&, int w, int h );
    void drawTriangle( Point&, Point&, Point&);
    void renderDrawing( const std::string& name, const std::string& format = "BMP" );

    void acceptCommand( const std::shared_ptr<DrawingCommand>& );
    ~PictureCreator();
private:
    void drawLine( Point& P1, Point& P2);
    PictureCreatorImpl* pImpl;

    enum format_t
    {
        F_BMP,
        F_JPG,
        F_PNG
    };
    std::map< std::string, format_t> str2format
    {
        {"BMP",F_BMP},
        {"JPG",F_JPG},
        {"PNG",F_PNG}
    };

};


#endif /* PICTURE_CREATOR_H_ */