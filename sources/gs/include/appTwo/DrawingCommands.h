#ifndef DRAWING_COMMANDS_H_
#define DRAWING_COMMANDS_H_
#include <string>
#include <memory>

#include "appTwo/PictureCreator.h"
#include "appTwo/DrawingCommandParser.h"


class DCSetWidth: public DrawingCommand
{
public:
    DCSetWidth( const std::string& cmd ) 
    {
        if( sscanf(cmd.c_str(), "%i", &w) < 1 )
            throw std::string("SetWidth: Invalid argument count");
        if( w < 1 )
            throw std::string("SetWidth: Invalid argument");
    }
    virtual void execute( PictureCreator* pPC) 
    {
        pPC->setWidth(w);
    }
private: 
    int w;
};

class DCSetHeight: public DrawingCommand
{
public:
    DCSetHeight(const std::string& cmd)
    {
        if( sscanf(cmd.c_str(), "%i",  &h) < 1 )
            throw std::string("SetHeight: Invalid argument count");
        if( h < 1 )
            throw std::string("SetHeight: Invalid argument");
    }
    virtual void execute( PictureCreator* pPC) 
    {
        pPC->setHeight(h);
    }
private: 
    int h;
};

class DCDrawRectangle: public DrawingCommand
{
public:
    DCDrawRectangle(const std::string& cmd ) 
    {
        if( sscanf(cmd.c_str(), "%i,%i,%i,%i", &P.x, &P.y, &w, &h) < 4 )
            throw std::string("DrawRectangle: Invalid argument count");
        if( h < 1 || w < 1 )
            throw std::string("DrawRectangle: Invalid argument");
    }
    virtual void execute( PictureCreator* pPC) 
    {
        pPC->drawRectangle(P,w,h);
    }
private: 
    PictureCreator::Point P;
    int w,h;
};


class DCDrawTriangle: public DrawingCommand
{
public:
    DCDrawTriangle( const std::string& cmd)
    {
        if( sscanf(cmd.c_str(), "%i,%i,%i,%i,%i,%i", &P1.x, &P1.y, &P2.x, &P2.y, &P3.x, &P3.y ) < 6 )
            throw std::string("DrawTriangle: Invalid argument count");
    }
    virtual void execute( PictureCreator* pPC) 
    {
        pPC->drawTriangle(P1,P2,P3);
    }
private: 
    PictureCreator::Point P1,P2,P3;
};
#include <iostream>
class DCRender: public DrawingCommand
{
public:
    DCRender( const std::string& cmd )
    {
        fp.resize(100);
        format.resize(3);
        if( sscanf(cmd.c_str(), "%s %s", (char*)fp.data(), (char*)format.data()  ) < 1 )
            throw std::string("Render: Invalid argument");
    }
    virtual void execute( PictureCreator* pPC) 
    {
        pPC->renderDrawing(fp, format);
    }
private:
    std::string fp;
    std::string format;
};





#endif /* DRAWING_COMMANDS_H_ */