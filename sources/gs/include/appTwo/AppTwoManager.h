#ifndef APPTWO_MANAGER_H_
#define APPTWO_MANAGER_H_

#include "connectors/ConnectionContext.h"

class AppTwoManager
{
public:
    AppTwoManager( ConnectionContext* pCC): pConnector(pCC){}
    void run() ;
private:
    ConnectionContext* pConnector;
};

#endif /* APPTWO_MANAGER_H_ */