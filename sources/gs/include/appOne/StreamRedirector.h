#ifndef STDSTREAM_REDIRECTORS_H_
#define STDSTREAM_REDIRECTORS_H_
#include <iostream>
#include <fstream>



class CLogRedirector
{
public:
    static CLogRedirector* redirStdStreamInstace( const std::string& path )
    {
        if( pInstance == nullptr )
        {
            // isActive 
            ///> TODO synch prmit
            if( pInstance == nullptr )
            {
                pInstance = new CLogRedirector( path);
                isActive = true;
            }
        }
        return pInstance;
    }
    ~CLogRedirector()
    {
        restore();
        delete pInstance;
        pInstance = nullptr;
    }
private:
    CLogRedirector( const std::string& path )
    {
        initialzie(path);
    }
    void restore()
    {
        std::clog.rdbuf(backup);        // restore clog's original streambuf
        filestr.close();
    }
    void initialzie( const std::string& path)
    {        
        filestr.open (path);
        if( !filestr.good() )
            throw std::string("Critical ERROR: failed to open form file: " + path) ;
            
        backup = std::clog.rdbuf();     // back up clog's streambuf
        psbuf = filestr.rdbuf();     // get file's streambuf
        std::clog.rdbuf(psbuf);         // assign streambuf to clog
    }
    static bool isActive;
    static CLogRedirector* pInstance;

    std::streambuf *psbuf, *backup;
    std::ofstream filestr;
};



class CInRedirector
{
public:
    static CInRedirector* redirStdStreamInstace( const std::string& path )
    {
        if( pInstance == nullptr )
        {
            // isActive 
            ///> TODO synch prmit
            if( pInstance == nullptr )
            {
                pInstance = new CInRedirector( path);
                isActive = true;
            }
        }
        return pInstance;
    }
    ~CInRedirector()
    {
        restore();
        delete pInstance;
        pInstance = nullptr;
    }
private:
    CInRedirector( const std::string& path )
    {
        initialzie(path);
    }
    void restore()
    {
        std::cin.rdbuf(backup);        // restore clog's original streambuf
        filestr.close();
    }
    void initialzie( const std::string& path)
    {
        filestr.open (path);
        if( !filestr.good() )
            throw std::string("Critical ERROR: failed to open form file: " + path) ;
            
        backup = std::cin.rdbuf();     // back up clog's streambuf
        psbuf = filestr.rdbuf();     // get file's streambuf
        std::cin.rdbuf(psbuf);         // assign streambuf to clog
    }
    static bool isActive;
    static CInRedirector* pInstance;

    std::streambuf *psbuf, *backup;
    std::ifstream filestr;
};

#endif /* STDSTREAM_REDIRECTORS_H_ */