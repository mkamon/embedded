#ifndef APPONE_MANAGER_H_
#define APPONE_MANAGER_H_

#include "connectors/ConnectionContext.h"
class AppOneManager
{
public:
    AppOneManager( ConnectionContext* pCC): pConnector(pCC){}
    void run( const std::string& filePath) ;
private:
    bool setupInputOutputStreams(const std::string& filePath);
    bool getNextCommand( std::string& msg);
    void processResponse( const std::string msg);
    void terminateConnection();

    ConnectionContext* pConnector;
};

#endif /* APPONE_MANAGER_H_ */