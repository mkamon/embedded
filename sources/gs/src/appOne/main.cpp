#include <unistd.h> 
#include <iostream>

#include "connectors/DoubleWayConnector.h"
#include "appOne/AppOneManager.h"
#include "appOne/Paths.h"

void showHelp();

int main(int argc, char* argv[])
{
  int c;
  std::string filePath;
  while ((c = getopt (argc, argv, "f:h")) != -1)
  {
      switch (c)
      {
          case 'h':
              showHelp();
              return 0;
          case 'f':
              filePath = workingSourceDir + optarg;
              break;
          default:
              std::cout << "Invalid option passed\n";
              break;
      }
  }

  char *my_args[4];
  pid_t pid;
  
  my_args[0] = &execPath[0];  

	int p[2];  
  DoubleWayConnector<100> DWConnector;
  DWConnector.getOtherEndRWIds( p[0], p[1] );

	my_args[1] = (char*)&p[0];
	my_args[2] = (char*)&p[1];
	my_args[3] = NULL;
  
  
  switch ((pid = fork()))
  {
    case -1:
      /* Fork() has failed */
      perror("If this prints, fork() must have failed");
      break;
    case 0:
      /* This is processed by the child */
      execvp (my_args[0], my_args);
      perror("If this prints, execv() must have failed");
      exit(EXIT_FAILURE);
      break;
    default:
      {
      /* This is processed by the parent */
        AppOneManager Manager(&DWConnector);
        Manager.run(filePath);
      }
      break;
  }
  
  std::cout << "AppOne Finished\n";
  return 0;

}

void showHelp()
{
  std::cout  
    << "The following is simple program for drawing rectangles and triangles in BMP format.\n"
    << "Application accepts command with both console and file input. For the latter use -f providing"
    << "name of file located in project's main directory.\n"
    << "The commands shall be formatted as follows:\n"
    << "\t SET_WIDTH W\n"
      << "\t\t @brief Sets the width of rendered picture\n"
      << "\t\t @param[in] W - integer argument\n"
    << "\t SET_HEIGHT H\n"
      << "\t\t @brief Sets the height of rendered picture\n"
      << "\t\t @param[in] H - integer argument\n"
    << "\t DRAW_RECTANGLE X,Y,W,H\n"
      << "\t\t @brief Draws rectangle with upper-left corner(X,Y), width(W) and height(H)\n"
      << "\t\t @param[in] X - integer argument\n"
      << "\t\t @param[in] Y - integer argument\n"
      << "\t\t @param[in] W - integer argument\n"
      << "\t\t @param[in] H - integer argument\n"
    << "\t DRAW_TRIANGLE X1,Y1,X2,Y2,X3,Y3\n"
      << "\t\t @brief Draws triangle spanned by points (X1,Y1),(X2,Y2),(X3,Y3)\n"
      << "\t\t @param[in] X1 - integer argument\n"
      << "\t\t @param[in] Y1 - integer argument\n"
      << "\t\t @param[in] X2 - integer argument\n"
      << "\t\t @param[in] Y2 - integer argument\n"
      << "\t\t @param[in] X3 - integer argument\n"
      << "\t\t @param[in] Y3 - integer argument\n"
    << "\t RENDER NAME FORMAT\n"
      << "\t\t @brief Renders picture and saves it in NAME.FORMAT file\n"
      << "\t\t @param[in] NAME - character string argument\n"
      << "\t\t @param[in] FORMAT - [optional] character string argument (BMP, PNG)\n"
    << "\t END\n"
      << "\t\t @brief Finishes the program\n";

}
