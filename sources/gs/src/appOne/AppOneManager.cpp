#include "appOne/AppOneManager.h"
#include "appOne/StreamRedirector.h"
#include <unistd.h> 
#include <iostream>

void AppOneManager::run( const std::string& filePath) 
    {
        while( true )
        {
            std::string msg;
            if( !setupInputOutputStreams(filePath) ||
                !getNextCommand(msg) )
                break;
            pConnector->sendMessage(msg);
            pConnector->readMessage(msg);
            processResponse(msg);
        }   
        terminateConnection();        
    }

bool AppOneManager::setupInputOutputStreams( const std::string& filePath )
    {
        CLogRedirector::redirStdStreamInstace( "logFile.txt");
        try{
            if( !filePath.empty() )
                CInRedirector::redirStdStreamInstace( filePath );
        }
        catch( const std::string& e ){
            std::clog << e << '\n' ; 
            return false;
        }
        return true;
    }

bool AppOneManager::getNextCommand( std::string& msg)
    {
        getline(std::cin, msg, '\n');
        if( msg.empty() || msg == "END")
            return false; 
        std::clog << "New command:" << msg << "\n";
        return true;
    }

void AppOneManager::processResponse( const std::string msg)
    {
        if( msg != "OK")
        {
            std::clog << msg << "\n";
            std::cout << "Error occured, see logFile for more info\n";
        }
    }

void AppOneManager::terminateConnection()
    {
        std::clog << "Terminating connection\n"; 
        pConnector->sendMessage("END");
    }