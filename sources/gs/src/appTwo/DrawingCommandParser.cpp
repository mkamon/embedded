#include "appTwo/DrawingCommandParser.h"
#include "appTwo/DrawingCommands.h"
#include <map>
#include <string>
enum cmd_t 
{
    CMD_SET_WIDTH = 10,
    CMD_SET_HEIGHT,
    CMD_DRAW_RECTANGLE,
    CMD_DRAW_TRIANGLE,
    CMD_RENDER
};

std::map<std::string, cmd_t> str2cmd
{
    {"SET_WIDTH", CMD_SET_WIDTH},
    {"SET_HEIGHT", CMD_SET_HEIGHT},
    {"DRAW_RECTANGLE", CMD_DRAW_RECTANGLE},
    {"DRAW_TRIANGLE", CMD_DRAW_TRIANGLE},
    {"RENDER", CMD_RENDER}    
} ;

// std::map<cmd_t, int> expectedArgsCnt
// {
//     { CMD_SET_WIDTH, 1},
//     { CMD_SET_HEIGHT, 1},
//     { CMD_DRAW_RECTANGLE, 4},
//     { CMD_DRAW_TRIANGLE, 6},
//     { CMD_RENDER, 1}    
    
// };


std::shared_ptr<DrawingCommand> DrawingCommandParser::parseCmd( const std::string& cmd)
    {        
        size_t pos = cmd.find(" ");
        std::string cmdName = cmd.substr(0, pos );
        std::string cmdArgs = cmd.substr(pos  );
        
        if( str2cmd.count(cmdName) == 0 )
            throw std::string("Unknown command: "+cmdName);

        std::shared_ptr<DrawingCommand> pCMD;
        switch( str2cmd[cmdName] )
        {
            case CMD_SET_WIDTH:
                pCMD = std::make_shared<DCSetWidth>( cmdArgs );
                break;
            case CMD_SET_HEIGHT:
                pCMD = std::make_shared<DCSetHeight>( cmdArgs); 
                break;
            case CMD_DRAW_RECTANGLE:
                pCMD = std::make_shared<DCDrawRectangle>( cmdArgs ); 
                break;
            case CMD_DRAW_TRIANGLE:
                pCMD = std::make_shared<DCDrawTriangle>( cmdArgs ); 
                break;
            case CMD_RENDER:
                pCMD = std::make_shared<DCRender>( cmdArgs ); 
                break;
            default :
                throw std::string("Unknown command: "+cmdName); 
        };
        return pCMD;
    }