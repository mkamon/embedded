#include "appTwo/PictureCreator.h"
#include "appTwo/DrawingCommandParser.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include <vector>

class PictureCreatorImpl
{
public: 
    int w = 0;
    int h = 0;
    SDL_Surface * image = nullptr;
    std::vector<SDL_Rect *> rects;
};


PictureCreator::PictureCreator()
    : pImpl( new PictureCreatorImpl() )
    {

    }
PictureCreator::~PictureCreator()
    {
        for( auto rect: pImpl->rects)
            delete rect;
        if( pImpl->image != nullptr ) 
            delete pImpl->image;
        delete pImpl;
    }
void PictureCreator::setWidth( int w)
    {
        pImpl->w=w;
    }
void PictureCreator::setHeight( int h)
    {
        pImpl->h=h;
    }
void PictureCreator::drawRectangle( Point& P, int w, int h)
    {
        pImpl->rects.push_back( new SDL_Rect{ P.x, P.y, w, h } );
    }
void PictureCreator::drawTriangle( Point& P1, Point& P2, Point& P3)
    {
        drawLine(P1,P2);
        drawLine(P1,P3);
        drawLine(P2,P3);
    }

void PictureCreator::acceptCommand( const std::shared_ptr<DrawingCommand>& cmd )
    {
        cmd->execute(this);
    }

void PictureCreator::renderDrawing( const std::string& name, const std::string& format )
    {
        if( pImpl->w < 1 || pImpl->h < 1 )
            throw std::string("Render: Invalid plane size");
        

        pImpl->image = SDL_CreateRGBSurface(0, pImpl->w, pImpl->h, 32, 0, 255, 0, 0);  
        Uint32 color = SDL_MapRGB(pImpl->image->format, 255, 255 ,255); 
        SDL_FillRect(pImpl->image, 0, color);
        for( auto rect: pImpl->rects)
        {
            SDL_FillRect(pImpl->image, rect, SDL_MapRGB(pImpl->image->format, 255, 0, 0));
            delete rect;
            rect = nullptr;
        }
        pImpl->rects.clear();
        
        format_t f = ( str2format.count( format) == 0 ? F_BMP : str2format.at(format) );
        switch (f)
        {
        case F_BMP:
            SDL_SaveBMP(pImpl->image, std::string(name).c_str() );
            break;
        // case F_JPG:
        //     IMG_SaveJPG(pImpl->image, std::string(name).c_str(), 100 );
        //     break;
        case F_PNG:
            IMG_SavePNG(pImpl->image, std::string(name).c_str() );
            break;
        default:
            SDL_SaveBMP(pImpl->image, std::string(name).c_str() );
            break;
        }
        SDL_FreeSurface(pImpl->image);
        pImpl->image = nullptr;
    }

void PictureCreator::drawLine( Point& P1, Point& P2)
    {
        int dx = P2.x - P1.x;
        int dy = P2.y - P1.y;

        for( int x=P1.x; x<=P2.x; x++ )
        {
            int y = P1.y + dy*( x - P1.x) / dx;
            pImpl->rects.push_back( new SDL_Rect{ x, y, 2, 2 } );
        }

    }