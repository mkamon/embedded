#include "appTwo/AppTwoManager.h"
#include "appTwo/DrawingCommandParser.h"
#include "appTwo/PictureCreator.h"
#include <iostream>

void AppTwoManager::run()
{
    PictureCreator Creator;
    std::string inStr; 
    while( true )
    {
        pConnector->readMessage(inStr);
        if( inStr == "END" )
            break;
            
        try{
            auto command = DrawingCommandParser::parseCmd(inStr);
            Creator.acceptCommand(command);
        }
        catch( const std::string& e)
        {
            pConnector->sendMessage(e);
            continue;
        }
        catch(...)
        {
            pConnector->sendMessage("SOMETHING WENT WRONG\n"); 
            continue;
        }
        pConnector->sendMessage("OK"); 
    }
}