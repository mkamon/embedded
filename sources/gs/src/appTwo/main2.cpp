#include "connectors/DoubleWayConnector.h"
#include "appTwo/AppTwoManager.h"
#include <iostream>

int main(int argc, char* argv[] )
{
    if( argc != 3 )
    {
        std::cout << "AppTwo: Critical failure, Aborting...\n";
        return -1; 
    }

    DoubleWayConnector<100> DWConnector(*argv[1], *argv[2]) ;
    AppTwoManager Manager( &DWConnector );
    Manager.run();
     std::cout << "AppTwo: Finished\n";
    return 0;
}



