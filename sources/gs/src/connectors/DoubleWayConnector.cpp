#include "connectors/DoubleWayConnector.h"
#include <unistd.h> 


template< size_t BS >
DoubleWayConnector<BS>::DoubleWayConnector()
    {
        if (pipe(thisEndRWIds) < 0 || pipe(otherEndRWIds) < 0) 
            throw std::string("Connector between programs failed"); 
        
        int temp = thisEndRWIds[0];
        thisEndRWIds[0]  = otherEndRWIds[0]; 
        otherEndRWIds[0] = temp;
    }

template< size_t BS >
DoubleWayConnector<BS>::DoubleWayConnector( int readId, int writeId )
    {
        thisEndRWIds[0] = readId;
        thisEndRWIds[1] = writeId;
        otherEndRWIds[0] = -1;
        otherEndRWIds[1] = -1;
    }

template< size_t BS >
DoubleWayConnector<BS>::~DoubleWayConnector()
    {
        if(  otherEndRWIds[0] == -1 )
        {
            close( thisEndRWIds[0] );
            close( thisEndRWIds[1] );
            close( otherEndRWIds[0] );
            close( otherEndRWIds[1] );
        }
    }


template< size_t BS >
void DoubleWayConnector<BS>::getOtherEndRWIds( int& readId, int& writeId ) 
    {
        readId = otherEndRWIds[0];
        writeId= otherEndRWIds[1];
    }

template< size_t BS >
void DoubleWayConnector<BS>::sendMessage( const std::string& msg)
    {
	    write(thisEndRWIds[1], msg.data() , msg.size() );
    }

template< size_t BS >
void DoubleWayConnector<BS>::readMessage( std::string& msg)
    {
        size_t size = read(thisEndRWIds[0], (void*)buff.data(), buff.size() );
        msg.assign( buff.data(), buff.data() + size );
    }

template class DoubleWayConnector<20>;
template class DoubleWayConnector<100>;